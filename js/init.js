(function(exports) {
    'use strict';
    
    //initialize the app
    var settings = {
        Model: JSONIGOLGIModel,
        PlayerView: PlayerView,
        PlaylistView: PlaylistPlayerView,
        dataURL: "https://mw.igocast.com/apiv2/channels/list",
        showSearch: true,
        displayButtons: false,
        controlsHideTime: 3000,
        payload :{
            auth:"auth=cZeTgCdDFpSd0S90fXnr+5Oe76mQVIHArfy6If2o6W4m1hUTGYxXvHkhSxWzbF0h+fOavVZsBTClrK1EdLrqcksk+IftCEJWTkiYlU0vXexht7v2VlS+gupNjJ+Q7QBOsbwecAF9YN/2XD/SHnDZlSBJ7wFJVT/IRzWipW7YaRI=",
            "sec-fetch-mode": "cors",
            accept: "application/json"

        }

    };

    exports.app = new App(settings);
}(window));
