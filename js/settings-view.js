/* Left Nav View
 *
 * Handles the display of main selectable categories on the
 * left side of the screen
 *
 */
(function (exports) {
	"use strict";

	//gloabl constants
	var CONTAINER_SCROLLING_LIST    = "#left-nav-scrolling-list",

		SETTINGS_BUTTN_ID              = "#settings-btn-id",

		SETTINGS_BUTTN_ACTIVE_CLASS    = "selected-menuitem",

		ACTIVE_SETTINGS                = "active",

		SETTINGS_MENU_ID               = "setting-menu-id",

		CLASS_MENU_ITEM_SELECTED    = "leftnav-list-item-selected",

		CLASS_MENU_ITEM_HIGHLIGHTED = "leftnav-list-item-highlighted",

		CLASS_MENU_ITEM_CHOSEN      = "leftnav-list-item-chosen";

	/**
	 * @class LeftNavView
	 * @description The left nav view object, this handles everything about the left nav menu.
	 */
	function SettingsView() {
		// mixin inheritance, initialize this as an event handler for these events:
		Events.call(this, ['exit', 'deselect', 'indexChange', 'select', 'makeActive', 'loadComplete', 'leftBounce', 'RightBounce']);

		this.scrollingContainerEle = null;
		this.leftNavContainerEle   = null;
		this.currentSelectionEle   = null;
		this.translateAmount       = null;
		this.currSelectedIndex     = 0;
		this.confirmedSelection    = 0;
		this.isDisplayed           = false;
		this.leftNavItems          = [];
		this.searchUpdated         = false;
		this.transformStyle = utils.vendorPrefix('Transform');

		this.settingsMenuItemData =[
			{
				name: "Dark theme",
				selected:true,
				active:true,
				id:1
			},
			{
				name: "Sub Title",
				selected:false,
				active:false,
				id:2
			}
		];
		this.selectedMenuId = 1;

		this.resetMenuSelected = function () {
			this.selectedMenuId = 1;
		};


		this.settingsActivated = false;

		//jquery variables
		this.$el = null;
		this.$menuItems            = [];

		this.fadeOut = function() {
			this.$el.fadeOut();
		};

		this.fadeIn = function() {
			this.$el.fadeIn();
		};

		/**
		 * Hides the left nav view
		 */
		this.hide = function () {
			this.$el.hide();
		};

		/**
		 * Display the left nav view
		 */
		this.show = function () {
			this.$el.show();
		};

		/**
		 * Change the style of the selected element to selected
		 * @param {Element} ele currently selected element
		 */
		this.setSelectedElement = function (ele) {
			ele = ele || this.currentSelectionEle;

			//remove chosen class if it's there
			if($(ele).hasClass(CLASS_MENU_ITEM_CHOSEN)) {
				$(ele).removeClass(CLASS_MENU_ITEM_CHOSEN);
			}

			//remove highlighted class if it's there
			var highlightedEle = $("." + CLASS_MENU_ITEM_HIGHLIGHTED);
			if(highlightedEle) {
				highlightedEle.removeClass(CLASS_MENU_ITEM_HIGHLIGHTED);
				this.leftNavContainerEle.classList.remove('leftnav-collapsed-highlight');
			}

			$(ele).addClass(CLASS_MENU_ITEM_SELECTED);
		};

		/**
		 * Change the style of the current left nav element to show it hightlight
		 * before the user must select to view the other menu items in the list
		 * @param {Element} ele currently selected element
		 */
		this.setHighlightedElement = function (ele) {
			ele = ele || this.currentSelectionEle;

			$(ele).removeClass(CLASS_MENU_ITEM_CHOSEN);
			$(ele).addClass(CLASS_MENU_ITEM_HIGHLIGHTED);
			this.leftNavContainerEle.classList.add('leftnav-collapsed-highlight');
		};

		this.menuActiveFlag = function () {
			this.settingsActivated = true;
		};

		this.menuDeActiveFlag = function () {
			this.settingsActivated = false;
		};

		this.activateSettingMenuBar = function () {
			if($(SETTINGS_BUTTN_ID).hasClass(SETTINGS_BUTTN_ACTIVE_CLASS)) {
				this.menuActiveFlag();
				this.RemoveMenuActivation();
				this.resetMenuSelection();
				console.log(this.settingsActivated);
				$("#"+SETTINGS_MENU_ID).addClass(ACTIVE_SETTINGS);
			}

		};


		/**
		 * Handle touch-selection of an item
		 */
		this.handleListItemSelection = function(e) {
			if(!this.isDisplayed) {
				this.trigger('makeActive');
			} else {
				this.setCurrentSelectedIndex($(e.target).parent().index());
				this.confirmNavSelection();
			}
		}.bind(this);

		/**
		 * Creates the left nav view from the template and appends it to the given element
		 * @param {Element} $el the application container
		 * @param {Array} catData category data
		 * @param {integer} startIndex initial item to select
		 */
		this.render = function ($el) {

			var leftNavStrings = [];

			var html = utils.buildTemplate($("#app-settings-template"), {
				settingsData:this.settingsMenuItemData
			});
			$el.append(html);
			this.$el = $el.children().last();




			//set default selected item
			//this.setSelectedElement(this.currentSelectionEle);

			//this.shiftNavScrollContainer();

			//register touch handlers for the left-nav items


			//send loadComplete event
			this.trigger('loadComplete');
		};


		/**
		 * Key event handler
		 * handles controls: LEFT : Return to last category
		 *                   RIGHT: Load new category and hide nav
		 *                   UP: Move selection up
		 *                   DOWN: Move selection down
		 *                   BACK: Exit app
		 * @param {event} the keydown event
		 */
		this.handleControls = function (e) {
			console.error(e.keyCode);
			console.error(e.type);
			if (e.type === 'swipe') {
				if(!this.settingsActivated) { return; }
				if(e.keyCode === touches.DOWN) {
					this.incrementCurrentSelectedIndex(-1);
				} else if(e.keyCode === touches.UP) {
					this.incrementCurrentSelectedIndex(1);
				}
			} else if (e.type === 'buttonpress') {
				switch (e.keyCode) {
					case buttons.UP:
						if(this.settingsActivated) {
							this.incrementCurrentSelectedIndex(-1);
						} else{
							this.trigger('leftBounce');
						}
						break;
					case buttons.DOWN:
						if(this.settingsActivated){
							this.incrementCurrentSelectedIndex(+1);
						} else{
							this.trigger('RightBounce');
						}
						break;
					case buttons.LEFT:
					case buttons.BACK:
						if(this.settingsActivated){
							this.removeSettinsMenu();
						} else {
							this.trigger('leftBounce');
						}
						break;
					case buttons.SELECT:
						if(this.settingsActivated) {
							this.changeSettingsValue();
						} else{
							this.activateSettingMenuBar();
						}
						break;
					case buttons.RIGHT:
						if(this.settingsActivated) {
							this.incrementCurrentSelectedIndex(+1);
						} else{
							this.trigger('RightBounce');
						}
						break;
				}
			} else if (e.type === 'buttonrepeat') {
				switch (e.keyCode) {
					case buttons.UP:
						if(this.settingsActivated) {
							this.incrementCurrentSelectedIndex(-1);
						}
						break;
					case buttons.DOWN:
						if(this.settingsActivated) {
							this.incrementCurrentSelectedIndex(1);
						}
						break;
				}
			}

		}.bind(this);

		this.resetMenuSelection = function() {
			var currentIndex = this.selectedMenuId;
			this.resetMenuSelected();
			$("#select-setting-"+currentIndex).removeClass("selected");
			$("#select-setting-"+this.selectedMenuId).addClass("selected");
		};

		/**
		 * Increment the index of the currently selected item
		 * relative to the last selected index.
		 * @param {number} direction to move the left nav
		 */
		this.incrementCurrentSelectedIndex = function(direction) {
			var currentIndex = this.selectedMenuId;
			$("#select-setting-"+currentIndex).removeClass("selected");
			if(direction > 0) {
				this.selectedMenuId +=1;
			} else{
				this.selectedMenuId -=1;
			}
			if (this.selectedMenuId > this.settingsMenuItemData.length) {
				this.resetMenuSelected();
			} else if(this.selectedMenuId < 1) {
				this.selectedMenuId = this.settingsMenuItemData.length;
			}
			$("#select-setting-"+this.selectedMenuId).addClass("selected");
		};
		this.changeSettingsValue = function() {
			//console.log(this.settingsMenuItemData[this.selectedMenuId-1]);
			if($("#select-setting-toggle-"+ this.selectedMenuId).hasClass("active")){
				$("#select-setting-toggle-"+ this.selectedMenuId).removeClass("active");
			}else {
				$("#select-setting-toggle-"+ this.selectedMenuId).addClass("active");
			}
		};

		this.removeSettinsMenu = function () {
			this.menuDeActiveFlag();
			$("#"+SETTINGS_MENU_ID).removeClass(ACTIVE_SETTINGS);
			this.trigger('RightBounce');
		};

		/**
		 * Explicity set the current selected index
		 * @param {Number} index the index of the item
		 */
		this.setCurrentSelectedIndex = function(index) {
			this.currSelectedIndex = index;
			this.selectLeftNavItem();
		};

		/**
		 * If the selection has changed update the category,
		 * otherwise just deselect the left-nav menu
		 */
		this.confirmNavSelection = function() {
			if(this.confirmedSelection !== this.currSelectedIndex) {
				// switch the current view state to the main content view
				var isObject = typeof this.leftNavItems[this.currSelectedIndex] === "object";
				var emptySearch = isObject && (this.leftNavItems[this.currSelectedIndex].currentSearchQuery === null || this.leftNavItems[this.currSelectedIndex].currentSearchQuery.length === 0);
				if (emptySearch) {
					return;
				}
				this.confirmedSelection = this.currSelectedIndex;
				this.trigger('select', this.currSelectedIndex);
			}
			else if (this.searchUpdated) {
				this.trigger('select', this.currSelectedIndex);
			}
			else {
				this.trigger('deselect');
			}
		};
		this.ActivateMenu = function () {
			if(!$(SETTINGS_BUTTN_ID).hasClass(SETTINGS_BUTTN_ACTIVE_CLASS)) {
				$(SETTINGS_BUTTN_ID).addClass(SETTINGS_BUTTN_ACTIVE_CLASS);
			}
		};

		this.RemoveMenuActivation = function () {
			$(SETTINGS_BUTTN_ID).removeClass(SETTINGS_BUTTN_ACTIVE_CLASS);
		};
		this.selectSettinsMenu = function() {
			this.ActivateMenu();
		};


		/**
		 * Moves the left nav selection in a direction, 1 is down, -1 is up
		 */
		this.selectLeftNavItem = function () {
			// update the left nav to the current selection and run the selection animation
			$(this.currentSelectionEle).removeClass(CLASS_MENU_ITEM_SELECTED);

			this.currentSelectionEle = this.$menuItems.eq(this.currSelectedIndex).children()[0];
			this.setSelectedElement(this.currentSelectionEle);

			this.shiftNavScrollContainer();

			//shade the elements farther away from the selection
			this.trigger('indexChange', this.currSelectedIndex);
		};

		/**
		 * Move the nav container as new items are selected
		 */
		this.shiftNavScrollContainer = function() {
			if(!this.translateAmount) {
				this.translateAmount = this.currentSelectionEle.getBoundingClientRect().height + 2;
			}

			//shift the nav as selection changes
			var translateHeight = 0 - (this.translateAmount * this.currSelectedIndex);
			this.scrollingContainerEle.style.webkitTransform = "translateY(" + translateHeight + "px)";
		};

	}

	exports.SettingsView = SettingsView;
}(window));
